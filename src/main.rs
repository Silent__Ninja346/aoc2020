mod common;
mod day_01;
mod day_02;
mod day_03;
mod day_04;
mod day_05;
mod day_06;

fn main() {
    println!("Day 1");
    day_01::part1();
    day_01::part2();
    day_01::part1_better();
    day_01::part2_better();

    println!("Day 2");
    day_02::part_1_and_2();

    println!("Day 3");
    day_03::part1();
    day_03::part2();

    println!("Day 4");
    day_04::part2();

    println!("Day 5");
    day_05::part_1_and_2();

    println!("Day 6");
    day_06::part1();
    day_06::part2();
}
