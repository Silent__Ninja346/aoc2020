#[derive(Debug)]
struct Passport {
    byr: Option<u32>,
    iyr: Option<u32>,
    eyr: Option<u32>,
    hgt: Option<u32>,
    hcl: Option<String>,
    ecl: Option<String>,
    pid: Option<u32>,
    cid: (),
}

impl Passport {
    fn new_empty() -> Self {
        Passport {
            byr: None,
            iyr: None,
            eyr: None,
            hgt: None,
            hcl: None,
            ecl: None,
            pid: None,
            cid: (),
        }
    }

    fn is_valid(&self) -> bool {
        match self {
            Passport {
                byr: Some(_),
                iyr: Some(_),
                eyr: Some(_),
                hgt: Some(_),
                hcl: Some(_),
                ecl: Some(_),
                pid: Some(_),
                cid: _,
            } => true,
            _ => false,
        }
    }

    fn parse_line(&mut self, line: &str) {
        let pairs: Vec<(&str, &str)> = line
            .split(' ')
            .map(|pair| -> (&str, &str) {
                let mut iter = pair.splitn(2, ':');
                (iter.next().unwrap(), iter.next().unwrap())
            })
            .collect();

        for (key, value) in pairs {
            match key {
                "byr" => self.byr = parse_byr(value),
                "iyr" => self.iyr = parse_iyr(value),
                "eyr" => self.eyr = parse_eyr(value),
                "hgt" => self.hgt = parse_hgt(value),
                "hcl" => self.hcl = parse_hcl(value),
                "ecl" => self.ecl = parse_ecl(value),
                "pid" => self.pid = parse_pid(value),
                "cid" => (),
                _ => panic!("despacito"),
            }

            /* For part 1
            match key {
                "byr" => self.byr = Some(1),
                "iyr" => self.iyr = Some(1),
                "eyr" => self.eyr = Some(1),
                "hgt" => self.hgt = Some(1),
                "hcl" => self.hcl = Some("hi".to_string()),
                "ecl" => self.ecl = Some("hi".to_string()),
                "pid" => self.pid = Some(1),
                "cid" => self.cid = Some(1),
                _ => panic!("despacito"),
            }
            */
        }
    }
}

fn parse_byr(value: &str) -> Option<u32> {
    let year = value.parse();
    if let Ok(byr) = year {
        if byr >= 1920 && byr <= 2002 {
            return Some(byr);
        }
    }

    None
}

fn parse_iyr(value: &str) -> Option<u32> {
    let year = value.parse();
    if let Ok(iyr) = year {
        if iyr >= 2010 && iyr <= 2020 {
            return Some(iyr);
        }
    }

    None
}

fn parse_eyr(value: &str) -> Option<u32> {
    let year = value.parse();
    if let Ok(eyr) = year {
        if eyr >= 2020 && eyr <= 2030 {
            return Some(eyr);
        }
    }

    None
}

fn parse_hgt(value: &str) -> Option<u32> {
    let hgt = value.trim_end_matches(char::is_alphabetic).parse();

    if let Ok(h) = hgt {
        if value.contains("cm") && (h >= 150 && h <= 193) {
            Some(h)
        } else if value.contains("in") && (h >= 59 && h <= 76) {
            Some(h)
        } else {
            None
        }
    } else {
        None
    }
}

fn parse_hcl(value: &str) -> Option<String> {
    let chars: Vec<char> = value.chars().collect();

    if chars.first().unwrap() != &'#' || chars.len() != 7 {
        return None;
    }

    for c in &chars[1..] {
        if !(c.is_ascii_lowercase() || c.is_numeric()) {
            return None;
        }
    }

    Some(value.to_string())
}

fn parse_ecl(value: &str) -> Option<String> {
    if let "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" = value {
        Some(value.to_string())
    } else {
        None
    }
}

fn parse_pid(value: &str) -> Option<u32> {
    if value.contains(char::is_alphabetic) || value.len() != 9 {
        None
    } else {
        if let Ok(p) = value.parse() {
            Some(p)
        } else {
            None
        }
    }
}

fn load_passports(filename: &str) -> Vec<Passport> {
    let mut passports = Vec::new();

    passports.push(Passport::new_empty());

    if let Ok(lines) = crate::common::read_lines(filename) {
        for line in lines {
            if let Ok(line) = line {
                if line.len() == 0 {
                    passports.push(Passport::new_empty());
                } else {
                    passports.last_mut().unwrap().parse_line(&line);
                }
            }
        }
    }

    passports
}

pub fn part2() {
    let filename = "inputs/day_04.txt";

    let passports = load_passports(filename);

    let mut count = 0;
    for p in &passports {
        if p.is_valid() {
            count += 1;
        }
    }

    println!("Valid Passports: {}", count);
}
