use std::collections::HashMap;
use std::collections::HashSet;

fn get_numbers(filename: &str) -> Vec<u32> {
    let mut numbers = Vec::new();
    if let Ok(lines) = crate::common::read_lines(filename) {
        lines.for_each(|line| {
            if let Ok(result) = line {
                numbers.push(result.parse().expect("failed to parse int"));
            }
        });
    } else {
        println!("failed to read in file");
    };

    return numbers;
}

pub fn part1() {
    let file_path = "inputs/day_01.txt";
    // Vec<u32>
    let numbers = get_numbers(file_path);
    'bp: for a in &numbers {
        for b in &numbers {
            if a + b == 2020 {
                println!("{} * {} = {}", a, b, a * b);
                break 'bp;
            }
        }
    }
}

pub fn part2() {
    let file_path = "inputs/day_01.txt";
    // Vec<u32>
    let numbers = get_numbers(file_path);
    'bp: for a in &numbers {
        for b in &numbers {
            for c in &numbers {
                if a + b + c == 2020 {
                    println!("{} * {} * {} = {}", a, b, c, a * b * c);
                    break 'bp;
                }
            }
        }
    }
}

pub fn part1_better() {
    // This should now be O(n)
    let file_path = "inputs/day_01.txt";
    // Vec<u32>
    let numbers = get_numbers(file_path);

    let mut search: HashMap<u32, u32> = HashMap::new();

    let target = 2020;

    for num in numbers {
        // try to get the value using num as a key
        if let Some(value) = search.get(&num) {
            // if we get it, our first number is the value, the second is
            // the target - the value
            let a = value;
            let b = target - value;
            println!("{} * {} = {}", a, b, a * b);
        } else {
            search.insert(target - num, num);
        }
    }
}

pub fn part2_better() {
    // this should now be O(n^2);
    let file_path = "inputs/day_01.txt";
    // Vec<u32>
    let numbers = get_numbers(file_path);

    let target = 2020;

    for (i, val) in numbers.iter().enumerate() {
        let mut map: HashSet<u32> = HashSet::new();
        let curr_sum: i32 = (target - val) as i32;

        for &j in &numbers[i..] {
            let sub = curr_sum - j as i32;

            if let Some(value) = map.get(&(sub as u32)) {
                let a = value;
                let b = val;
                let c = j;
                println!("{} * {} * {} = {}", a, b, c, a * b * c);
            } else {
                map.insert(j);
            }
        }
    }
}
