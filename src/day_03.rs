struct MountainMap {
    data: Vec<Vec<char>>,
    height: usize,
    width: usize,
}

impl MountainMap {
    fn get_at(&self, x: usize, y: usize) -> Result<char, &str> {
        if y >= self.height {
            Err("tried to lookup beyond the mountain")
        } else {
            Ok(self.data[y][x % self.width])
        }
    }

    fn count_path(&self, dx: usize, dy: usize) -> usize {
        let mut x = 0;
        let mut y = 0;
        let mut tree_count = 0;
        while let Ok(result_c) = self.get_at(x, y) {
            if result_c == '#' {
                tree_count += 1;
            }
            x += dx;
            y += dy;
        }
        tree_count
    }
}

fn load_map(filename: &str) -> MountainMap {
    let mut height = 0;
    let mut data: Vec<Vec<char>> = Vec::new();

    if let Ok(lines) = crate::common::read_lines(filename) {
        lines.for_each(|line| {
            if let Ok(row) = line {
                data.push(row.chars().collect());
                height += 1;
            }
        })
    }

    let width = data[0].len();

    MountainMap {
        data: (data),
        height: (height),
        width: (width),
    }
}

pub fn part1() {
    let map = load_map("inputs/day_03.txt");
    println!("Trees hit: {}", map.count_path(3, 1));
}

pub fn part2() {
    let map = load_map("inputs/day_03.txt");
    let path1 = map.count_path(1, 1);
    let path2 = map.count_path(3, 1);
    let path3 = map.count_path(5, 1);
    let path4 = map.count_path(7, 1);
    let path5 = map.count_path(1, 2);
    println!(
        "Trees multiplied: {}",
        path1 * path2 * path3 * path4 * path5
    );
}
